#! /bin/bash


case $1 in
-h | --help)
		echo "SHORT OPTION	LONG OPTION	DESCRIPTION"
		echo "-h		--help		show help"
		echo "-l		--location	show weather"
		;;		
-l | --location)
	curl "https://wttr.in/$2"
	;;
*)
	curl "https://wttr.in"
esac	




