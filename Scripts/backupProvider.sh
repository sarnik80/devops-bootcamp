#! /bin/bash


src=$1
dest=$2


if [ -z $src ] || [ -z $dest ]
then
	echo "Two Arguments needed"
	exit 1
fi

if [ -e $src ]
then
	cp -rv $src $dest
	echo "Backup Successful"
else
	echo "Source Path not exists : $src "
fi





